package com.example.admin.rxjavademo;

/**
 * Created by brucezz on 2017/6/13.
 * Zus_Android
 * Email: im.brucezz@gmail.com
 * GitHub: https://github.com/brucezz
 */

public final class Transformers {

    /**
     * 异步 do not use in Dao [data] layer
     */
    public static <T> AsyncTransformer<T> async() {
        return new AsyncTransformer<>();
    }

}

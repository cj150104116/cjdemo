package com.example.admin.rxjavademo;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

public class TypeReference<T> {

    private final Type type;

    public TypeReference() {
        Type superClass = getClass().getGenericSuperclass();

        type = ((ParameterizedType) superClass).getActualTypeArguments()[0];
    }

    public Type getType() {
        return type;
    }

    public final static Type LIST_STRING = new TypeReference<List<String>>() {
    }.getType();

    class Base {

    }

    class Sub extends Base {

    }

    Sub sub = new Sub();
    Base base = new Base();

    public void testSuper(Collection<? super Base> para) {
        para.add(new Sub());//编译通过
        para.add(new Base());//编译不通过
    }
}
